import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import { useState } from 'react'

const inter = Inter({ subsets: ['latin'] })

const INITIAL_STATE = {
  fcp: 0,
  si: 0,
  fmp: 0,
  tti: 0,
  fci: 0,
  lcp: 0,
  tbt: 0,
  cls: 0,
  device: 'mobile',
}
export default function Home() {
  const [state, setState] = useState(INITIAL_STATE)
  const { fcp, si, fmp, tti, fci, lcp, tbt, cls, device } = state
  const baseURL = `https://googlechrome.github.io/lighthouse/scorecalc/`
  const query = `#FCP=${fcp}&SI=${si}&FMP=${fmp}&TTI=${tti}&FCI=${fci}&LCP=${lcp}&TBT=${tbt}&CLS=${cls}&device=${device}&version=10`

  const handlerChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target
    setState({ ...state, [name]: value })
  }

  return (
    <div className={styles.container}>
      <h1>Preencha os campos</h1>
      <div className={styles.form}>
        <div className={styles.deviceContainer}>
          <h2 className={styles.deviceTitle}>Device</h2>
          <div className={styles.deviceInputs}>
            <label htmlFor="mobile">MOBILE</label>
            <input type="radio" id="mobile" name="device" value="mobile" onChange={handlerChange} checked />
          </div>
          <div className={styles.deviceInputs}>
            <label htmlFor="desktop">DESKTOP</label>
            <input type="radio" id="desktop" name="device" value="desktop" onChange={handlerChange} />
          </div>
        </div>
        <label htmlFor="fcp">{'FCP (Primeira Exibição de Conteúdo) *'}</label>
        <input className={styles.input} type="text" id="fcp" name="fcp" onChange={handlerChange} required />
        <label htmlFor="si">{'Speed Index – Índice de Velocidade *'}</label>
        <input className={styles.input} type="text" id="si" name="si" onChange={handlerChange} required />
        <label htmlFor="fmp">{'First Meaningful Paint (FMP) – Primeira Exibição Importante'}</label>
        <input className={styles.input} type="text" id="fmp" name="fmp" onChange={handlerChange} />
        <label htmlFor="tti">TTI - Time To Interactive – Tempo Para Interação</label>
        <input className={styles.input} type="text" id="tti" name="tti" onChange={handlerChange} />
        <label htmlFor="fci">FCI - First CPU Idle – Tempo para Interação.</label>
        <input className={styles.input} type="text" id="fci" name="fci" onChange={handlerChange} />
        <label htmlFor="lcp">LCP - Largest Contentful Paint*</label>
        <input className={styles.input} type="text" id="lcp" name="lcp" onChange={handlerChange} required />
        <label htmlFor="tbt">TBT - Total Blocking Time *</label>
        <input className={styles.input} type="text" id="tbt" name="tbt" onChange={handlerChange} required />
        <label htmlFor="cls">CLS - Cumulative Layout Shift *</label>
        <input className={styles.input} type="text" id="cls" name="cls" onChange={handlerChange} required />
        <a className={styles.button} href={`${baseURL}${query}`} target="_blank">
          Calcule a nota de desempenho
        </a>
      </div>
    </div>
  )
}
